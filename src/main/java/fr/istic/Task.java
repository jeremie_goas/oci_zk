package fr.istic;

import org.zkoss.zul.Combobox;
import org.zkoss.zul.ListModelList;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by goas on 12/01/16.
 */
public class Task {

    private String taskname;

    private  Combobox combobox = new Combobox();

    public Task() {
        List l = new ArrayList();
        l.add("TODO");
        l.add("DOING");
        l.add("DONE");
        combobox.setModel(new ListModelList<Object>(l));
        combobox.setButtonVisible(true);
    }

    public String getTaskname() {
        return taskname;
    }

    public void setTaskname(String taskname) {
        this.taskname = taskname;
    }

    public Combobox getCombobox() {
        return combobox;
    }

    public void setCombobox(Combobox combobox) {
        this.combobox = combobox;
    }
}
