package fr.istic;

import org.zkoss.bind.annotation.Init;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.metainfo.EventHandler;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MyViewModel extends SelectorComposer<Component> {

	@Wire
	private Button addBtn;

	@Wire
	private Textbox taskEditableText;

	@Wire
	private Listbox taskList;

	private List<Task> tasks = new ArrayList<Task>();

	@Listen("onClick = #addBtn")
	public void addTask() {
		if ( taskEditableText == null || taskList == null ){
			Messagebox.show( "Error on this application", "Error", Messagebox.OK, Messagebox.ERROR );
		}else{
			String text = taskEditableText.getValue();

			if ( text != null && !text.isEmpty() ){
				taskList.setItemRenderer(new ListitemRenderer<Task>() {
					public void render(final Listitem listitem, Task task, final int i) throws Exception {

						// First Column
						new Listcell(task.getTaskname()).setParent(listitem);

						// Second Column
						Listcell cell2 = new Listcell();
						task.getCombobox().setParent(cell2);
						cell2.setParent(listitem);

						// Third Column
						Button button = new Button("Remove");
						Listcell cell3 = new Listcell();
						button.setParent(cell3);
						cell3.setParent(listitem);

						button.addEventListener("onClick", new EventListener<Event>() {
							public void onEvent(Event event) throws Exception {
								tasks.remove(i);
								ListModel lm = new ListModelList(tasks, true);
								taskList.setModel(lm);
							}
						});

					}
				});

				Task t = new Task();
				t.setTaskname(text);
				tasks.add(t);
				ListModel lm = new ListModelList( tasks, true );
				taskList.setModel(lm);
			}else{
				Messagebox.show("The task name is empty", "Warning", Messagebox.OK, Messagebox.EXCLAMATION);
			}
		}
	}

	public Button getAddBtn() {
		return addBtn;
	}

	public void setAddBtn(Button addBtn) {
		this.addBtn = addBtn;
	}

	public Textbox getTaskEditableText() {
		return taskEditableText;
	}

	public void setTaskEditableText(Textbox taskEditableText) {
		this.taskEditableText = taskEditableText;
	}
}
